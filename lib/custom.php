<?php
/**
 * Custom functions
 */

// Register Theme Scripts
function theme_scripts() {
    // Disable roots
    wp_dequeue_style('roots_main');
    wp_dequeue_script('modernizr');
    wp_dequeue_script('roots_scripts');
    remove_action('wp_head', 'roots_jquery_local_fallback'); // TODO: add our own someday
    remove_filter('script_loader_src', 'roots_jquery_local_fallback');
    if (!is_admin() && current_theme_supports('jquery-cdn')) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false);
        wp_register_script('jquery_migrate', '//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js', array(), null, false);
        wp_enqueue_script('jquery_migrate');
    }

    // CSS
    wp_register_style('css_bootstrap', get_template_directory_uri() . '/minified/bootstrap.custom.min.css', array(), null, false);
    wp_register_style('css_app', get_template_directory_uri() . '/minified/app.min.css', array(), null, false);
    wp_enqueue_style('css_bootstrap');
    wp_enqueue_style('css_app');

    // JS
    wp_register_script('js_helper', get_template_directory_uri() . '/minified/helpers.min.js', array(), null, true);
    wp_register_script('js_vendor', get_template_directory_uri() . '/minified/vendor.min.js', array(), null, true);
    wp_register_script('js_app', get_template_directory_uri() . '/minified/app.min.js', array(), null, true);
    wp_enqueue_script('js_helper');
    wp_enqueue_script('js_vendor');
    wp_enqueue_script('js_app');

    // IE Specific
    add_action('wp_head', 'endeavr_head_ie');
}
add_action('wp_enqueue_scripts', 'theme_scripts', 100);

function endeavr_head_ie() {
    echo '<!--[if (gte IE 6)&(lte IE 9)]><link rel="stylesheet" type="text/css" href="/minified/ie.min.css"><script src="/minified/ie.vendors.min.js"></script><![endif]-->';
}

// Regsiter FontAwesome
function fontawesome_script() {
    wp_enqueue_style( 'fa-style', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(), '1.0', 'screen,projection');
}
add_action('wp_enqueue_scripts', 'fontawesome_script');

// Register Tiny MCE Buttons
function enable_more_buttons($buttons) {
    $buttons[] = 'hr';
    return $buttons;
}
add_filter("mce_buttons", "enable_more_buttons");

// DISABLE HEARTBEAT API
add_action( 'init', 'my_deregister_heartbeat', 1 );
function my_deregister_heartbeat() {
    global $pagenow;

    if ( 'post.php' != $pagenow && 'post-new.php' != $pagenow )
        wp_deregister_script('heartbeat');
}

// CAMPAIGN THEME
require_once "endeavr.churchamp/init.php";