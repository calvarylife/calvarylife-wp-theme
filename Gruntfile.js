'use strict';
module.exports = function(grunt) {

    grunt.initConfig({
//        jshint: {
//            options: {
//                jshintrc: '.jshintrc'
//            },
//            all: [
//                'Gruntfile.js',
//                'assets/js/*.js',
//                '!asssets/js/plugins/*.js',
//                '!asssets/js/bootstrap/*.js',
//                '!asssets/js/vendor/*.js',
//                '!asssets/js/ie/*.js',
//                '!assets/js/minified/*.js'
//            ]
//        },
        jshint: {
            options: {
                browser: true,
                force: true,
                jquery: true,
                globals: {
                    _: true,
                    messengerHelper: true,
                    audacityHelper: true,
                    numeral: true,
                    Tour: true,
                    introJs: true,
                    console: true,
                    Messenger: true,
                    TweenLite: true,
                    tourInfos: true,
                    animationHelper: true,
                    Power1: true,
                    Craft: true,
                    graph: true,
                    Chart: true,
                    confirm: true,
                    backstrechImgs: true,
                    NProgress: true,
                    tabsHelper: true,
                    backstrechHelper: true,
                    dynamicAlign: true,
                    workHelper: true,
                    profileHelper: true,
                    variablesHelper: true,
                    Modernizr: true,
                    FastClick: true,
                    preloaderHelper: true,
                    appHelper: true,
                    PxLoader: true,
                    networkModule: true,
                    managementModule: true,
                    brandsModule: true,
                    workModule: true,
                    skrollr: true,
                    capabilitiesSlideshow: true,
                    isMobile: true
                },
                ignores: [
                    'source/js/helper/misc.js'
                ]
            },
            dev: {
                options: {
                    curly: false,
                    undef: true
                },
                files: {
                    src: ['source/js/helper/*.js', 'source/js/pages/*.js', 'source/js/main.js']
                }
            }
        },

        less: {
            options: {
                report: 'min'
            },
            dev: {
                files: [
                    {src: ["source/less/bootstrap/custom/bootstrap.less"], dest: "minified/bootstrap.custom.min.css"},
                    {src: ["source/less/app.less"], dest: "minified/app.min.css"},
                    {src: ["source/less/ie.less"], dest: "minified/ie.min.css"}
                ],
                options: {
                    ieCompat: true
                }
            },
//            prod: {
//                files: [
//                    {src: ["source/less/bootstrap_custom.less"], dest: "public/minified/bootstrap.custom.min.css"},
//                    {src: ["source/less/app.less"], dest: "public/minified/app.min.css"},
//                    {src: ["source/less/ie/loader.less"], dest: "public/minified/ie.min.css"}
//                ],
//                options: {
//                    cleancss: true,
//                    compress: true,
//                    ieCompat: true,
//                    optimization: 2
//                }
//            }
        },














//        less: {
//            dist: {
//                files: {
//                    'assets/css/main.min.css': [
//                        'assets/less/app.less'
//                    ]
//                },
//                options: {
//                    compress: true,
//                    // LESS source map
//                    // To enable, set sourceMap to true and update sourceMapRootpath based on your install
//                    sourceMap: false,
//                    sourceMapFilename: 'assets/css/main.min.css.map',
//                    sourceMapRootpath: '/app/themes/roots/'
//                }
//            }
//        },
//        uglify: {
//            dist: {
//                files: {
//                    'assets/js/scripts.min.js': [
//                        'assets/js/plugins/bootstrap/transition.js',
//                        'assets/js/plugins/bootstrap/alert.js',
//                        'assets/js/plugins/bootstrap/button.js',
//                        'assets/js/plugins/bootstrap/carousel.js',
//                        'assets/js/plugins/bootstrap/collapse.js',
//                        'assets/js/plugins/bootstrap/dropdown.js',
//                        'assets/js/plugins/bootstrap/modal.js',
//                        'assets/js/plugins/bootstrap/tooltip.js',
//                        'assets/js/plugins/bootstrap/popover.js',
//                        'assets/js/plugins/bootstrap/scrollspy.js',
//                        'assets/js/plugins/bootstrap/tab.js',
//                        'assets/js/plugins/bootstrap/affix.js',
//                        'assets/js/plugins/*.js',
//                        'assets/js/_*.js'
//                    ]
//                },
//                options: {
//                    // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
//                    // sourceMap: 'assets/js/scripts.min.js.map',
//                    // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
//                }
//            }
//        },
        concat: {
            options: {
                separator: ';'
            },
            scripts: {
                files: [
                    {src: [
                        'source/js/helper/*.js'
                    ], dest: 'minified/helpers.min.js'},
                    {src: [
                        'source/js/modernizr-2.7.0.min.js',
                        'source/js/vendor/twitter_bootstrap/transition.js',
                        'source/js/vendor/twitter_bootstrap/alert.js',
                        'source/js/vendor/twitter_bootstrap/button.js',
                        'source/js/vendor/twitter_bootstrap/carousel.js',
                        'source/js/vendor/twitter_bootstrap/collapse.js',
                        'source/js/vendor/twitter_bootstrap/dropdown.js',
                        'source/js/vendor/twitter_bootstrap/modal.js',
                        'source/js/vendor/twitter_bootstrap/tooltip.js',
                        'source/js/vendor/twitter_bootstrap/popover.js',
                        'source/js/vendor/twitter_bootstrap/scrollspy.js',
                        'source/js/vendor/twitter_bootstrap/tab.js',
                        'source/js/vendor/twitter_bootstrap/affix.js',
                        'source/js/vendor/backstretch/jquery.backstretch.js',
                        'source/js/vendor/equalizer/equalizer.js',
                        'source/js/vendor/placeholder/placeholder.js',
                        'source/js/vendor/royalslider/jquery.royalslider.custom.min.js',
                        'source/js/vendor/underscore/underscore.js',
                        'source/js/vendor/underscore.string/underscore.string.js',
                        'source/js/vendor/waypoints/waypoints.js'
                    ], dest: 'minified/vendor.min.js'},
                    {src: [
                        'source/js/main.js',
                        'source/js/pages/*.js'
                    ], dest: 'minified/app.min.js'},
                    {src: [
                        'source/js/vendor/html5shiv/html5shiv-printshiv.js',
                        'source/js/vendor/selectivizr/selectivizr.js',
                        'source/js/vendor/respond/respond.js'
                    ], dest: 'minified/ie.vendor.js'},
                ]
            }
        },

        notify: {
            watch: {
                options: {
                    title: 'Compiled successfully!',
                    message: 'Continue... :)'
                }
            },
            ready: {
                options: {
                    title: 'Hi Sir,',
                    message: 'Time to code!'
                }
            }
        },

        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5,
                title: "Endeavr ChurchAmp"
            }
        },

        watch: {
            scripts: {
                files: ['source/js/**/*.js'],
                tasks: ['jshint:dev', 'concat', 'notify:watch'],
                options: {
                    spawn: false
                }
            },
            less: {
                files: ['source/less/**/*.less'],
                tasks: ['less:dev', 'notify:watch']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    'minified/*'
                ]
            }
        },
//        version: {
//            options: {
//                file: 'lib/scripts.php',
//                css: 'assets/css/main.min.css',
//                cssHandle: 'roots_main',
//                js: 'assets/js/scripts.min.js',
//                jsHandle: 'roots_scripts'
//            }
//        },
//        watch: {
//            less: {
//                files: [
//                    'assets/less/*.less',
//                    'assets/less/bootstrap/*.less',
//                    'assets/less/blocks/*.less',
//                    'assets/less/components/*.less',
//                    'assets/less/grid/*.less',
//                    'assets/less/ie/*.less',
//                    'assets/less/mixins/*.less',
//                    'assets/less/mq/*.less',
//                    'assets/less/pages/*.less',
//                    'assets/less/themes/*.less',
//                    'assets/less/variables/*.less',
//                    'assets/less/viewport/*.less'
//                ],
//                tasks: ['less', 'version']
//            },
//            js: {
//                files: [
//                    '<%= jshint.all %>'
//                ],
//                tasks: ['jshint', 'uglify', 'concat', 'version']
//            },
//            livereload: {
//                // Browser live reloading
//                // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
//                options: {
//                    livereload: true
//                },
//                files: [
//                    'assets/css/main.min.css',
//                    'assets/js/minified/*.js',
//                    'templates/*.php',
//                    '*.php'
//                ]
//            }
//        },
//        clean: {
//            dist: [
//                'assets/css/main.min.css',
//                'assets/js/minified/scripts.min.js'
//            ]
//        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-wp-version');

    // Register tasks
    grunt.registerTask('default', [
 //       'clean',
        'less',
//        'uglify',
        'concat',
        'notify:ready',
//        'version',
        'watch'
    ]);
//    grunt.registerTask('dev', [
//        //'watch'
//    ]);

};
