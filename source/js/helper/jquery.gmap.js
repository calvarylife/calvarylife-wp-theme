var mapHelper = {

    init: function() {
        this.loadMaps();
    },

    loadMaps: function() {
        if (typeof blockMaps != "undefined") {
            _.each(blockMaps, function(obj, index) {
                var map = new GMaps({
                    div: '#' + index,
                    lat: -12.043333,
                    lng: -77.028333,
                    zoom_changed: function() {
                        if (this.customLoaded == 0) {
                            this.customLoaded = 1;
                            this.setZoom(this.getZoom() - 1);
                        }
                    }
                });

                for (var i = 0; i<obj.length; i++) {
                    mapHelper.loadMarker(obj[i], map);
                };
                map.map.customLoaded = 0;
                map.fitZoom();
                mapHelper.vars.maps[index] = map;
            });
        }
    },

    loadMarker: function(data, map) {
        map.addMarker({
            lat: data.lat,
            lng: data.lng
        });
    },

    vars: {
        maps : {},
    }
};